package es.upm.babel.bytemanGenerator;

import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

public class RuleGenerator {
  
  private String name;
  private Class intClass;
  private Set<Class> userClasses;
  private Class userClass;
  private String packageName;
  private Set<Field> accessibleVars;
  private Set<String> ruleNames;
  Random rnd = new Random();
  
  /**
   * inClass is the class listing the resource operations to implement.
   * userClasses are the classes the user has provided to implement the operations.
   */
  public RuleGenerator(String name, String packageName, Class intClass, Set<Class> userClasses) {
    this.name = name;
    this.packageName = packageName;
    this.intClass = intClass;
    this.userClasses = userClasses;
    this.ruleNames = new HashSet<String>();
    
    boolean foundImplementation = false;

    for (Class userClass : userClasses) {
      if (intClass.isAssignableFrom(userClass)) {
        this.userClass = userClass;
        foundImplementation = true;
      }
    }
    
    if (!foundImplementation) {
      System.out.println("\nERROR: could not find class implementing "+intClass);
      System.exit(1);
    }

    this.accessibleVars = accessibleVars(userClass, userClasses);

    System.out.println
      (name+" has classes "+userClasses+" and variables "+accessibleVars);
  }
  
  private static Set<Field> accessibleVars(Class userClass, Set<Class> userClasses) {
    Set<Field> accessibleVars = new HashSet<>();
    for (Field f : fields(userClass)) {
      accessibleVars.add(f);
    }

    for (Class cl : userClasses) {
      for (Field f : staticFields(cl)) {
        System.out.println("\n*** WARNING: class "+cl+" has static field "+f+"\n");
        accessibleVars.add(f);
      }
    }
    return accessibleVars;
  }
  
  private String outputFieldAccessRules() {
    ArrayList<Method> methods = methods(userClass);
    ArrayList<Field> fields = fields(userClass);
    StringBuffer sb = new StringBuffer();
    String ruleName;
    if (fields.size() > 0) {
      for (Method m : methods) {
        for (Field f : accessibleVars) {
          if (f.getType().getName().equals("es.upm.babel.cclib.Monitor"))
            continue;
          ruleName = userClass.getSimpleName()+" "+m.getName()+" read "+f.getName();
          if (ruleNames.contains(ruleName)) {
            System.out.println("*** WARNING: duplicated rule name "+ruleName);
            //ruleName = ruleName + "_" + new Integer(rnd.nextInt(20000)).toString();
            //System.out.println("New name is "+ruleName);
          } else {
            ruleNames.add(ruleName);
            sb.append("RULE "+ruleName+"\n");
            sb.append("CLASS "+userClass.getName()+"\n");
            sb.append("METHOD "+m.getName()+"\n");
            sb.append("AT READ "+f.getName()+" ALL\nIF true\n");
            sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeRead($CLASS,$0,\""+m.getName()+"\",\""+f.getName()+"\")\n");
            sb.append("ENDRULE\n\n");
          }

          ruleName = userClass.getSimpleName()+" "+m.getName()+" write "+f.getName();
          if (ruleNames.contains(ruleName)) {
            System.out.println("*** WARNING: duplicated rule name "+ruleName);
            //ruleName = ruleName + "_" + new Integer(rnd.nextInt(20000)).toString();
            //System.out.println("New name is "+ruleName);
          } else {
            ruleNames.add(ruleName);
            sb.append("RULE "+ruleName+"\n");
            sb.append("CLASS "+userClass.getName()+"\n");
            sb.append("METHOD "+m.getName()+"\n");
            sb.append("AT WRITE "+f.getName()+" ALL\nIF true\n");
            sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeWrite($CLASS,$0,\""+m.getName()+"\",\""+f.getName()+"\")\n");
            sb.append("ENDRULE\n\n");
          }
        }
      }
    }
    return sb.toString();
  }

  private boolean isPrimitiveType(Type t) {
    return
      t.equals(Void.TYPE) ||
      t.equals(Short.TYPE) ||
      t.equals(Long.TYPE) ||
      t.equals(Double.TYPE) ||
      t.equals(Integer.TYPE) ||
      t.equals(Float.TYPE) ||
      t.equals(Byte.TYPE) ||
      t.equals(Character.TYPE) ||
      t.equals(Boolean.TYPE);
  }

  private String outputFieldCallRules() {
    ArrayList<Field> fields = fields(userClass);
    StringBuffer sb = new StringBuffer();
    Set<Class> classes = new HashSet<Class>();

    for (Field f : fields) {
      Class<?> cl = f.getType();
      classes.add(cl);
    }
        
    for (Class cl : classes) {
      if (cl.getName().matches("es.upm.babel.cclib.*")) continue;
      ArrayList<Method> methods = methods(cl);
      for (Method m : methods) {
        if (!Modifier.isPublic(m.getModifiers())) continue;
        int arity = m.getParameterTypes().length;
        sb.append("RULE "+m.getName()+" "+arity+" enter "+cl.getName()+"\n");
        if (cl.isInterface())
          sb.append("INTERFACE ^"+cl.getName()+"\n");
        else
          sb.append("CLASS "+cl.getName()+"\n");
        sb.append("METHOD "+m.getName()+"\n");
        sb.append("AT ENTRY\n");
        sb.append("IF callerMatches(\""+userClass.getName()+".*\",true,true)\n");
        sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeCall($CLASS,\""+m.getName()+"\",$*)\n");
        sb.append("ENDRULE\n\n");
      }
    }
    return sb.toString();
  }

  private String outputInterfaceRules() {
    StringBuffer sb = new StringBuffer();

    ArrayList<Constructor> constructors = constructors(userClass);
    for (Constructor cnst : constructors) {
      sb.append("RULE "+userClass.getSimpleName()+" enter\n");
      sb.append("CLASS "+userClass.getName()+"\n");
      sb.append("METHOD <init>\n");
      sb.append("AT ENTRY\nIF true\n");
      sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeNew($CLASS,$*)\n");
      sb.append("ENDRULE\n\n");
      
      sb.append("RULE "+userClass.getSimpleName()+" exit\n");
      sb.append("CLASS "+userClass.getName()+"\n");
      sb.append("METHOD <init>\n");
      sb.append("AT EXIT\nIF true\n");
      sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeNewReturn($CLASS,$*,$0)\n");
      sb.append("ENDRULE\n\n");
    }
    
    ArrayList<Method> methods = methods(intClass);
    for (Method m : methods) {
      sb.append("RULE "+m.getName()+" enter\n");
      sb.append("CLASS "+userClass.getName()+"\n");
      sb.append("METHOD "+m.getName()+"("+param_types(m)+")"+"\n");
      sb.append("AT ENTRY\nIF true\n");
      sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeCall($CLASS,\""+m.getName()+"\",$*)\n");
      sb.append("ENDRULE\n\n");
      
      sb.append("RULE "+m.getName()+" exit\n");
      sb.append("CLASS "+userClass.getName()+"\n");
      sb.append("METHOD "+m.getName()+"("+param_types(m)+")"+"\n");
      sb.append("AT EXIT\nIF true\n");
      sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeCallReturn($CLASS,\""+m.getName()+"\",$*,"+retValue(m)+")\n");
      sb.append("ENDRULE\n\n");
      sb.append("RULE "+m.getName()+" raises\n");
      sb.append("CLASS "+userClass.getName()+"\n");
      sb.append("METHOD "+m.getName()+"("+param_types(m)+")"+"\n");
      sb.append("AT EXCEPTION EXIT\nIF true\n");
      sb.append("DO es.upm.babel.bytemanGenerator.ByteManUtils.makeCallThrow($CLASS,\""+m.getName()+"\",$*,$^)\n");
      sb.append("ENDRULE\n\n");
    }
    
    return sb.toString();
  }
  
  private String param_types(Method m) {
    StringBuffer sb = new StringBuffer();
    for (Type t : m.getGenericParameterTypes()) {
      if (sb.length() > 0) sb.append(",");
      if (t instanceof Class) {
        Class cl = (Class) t;
        sb.append(cl.getName());
      } else throw new RuntimeException();
    }
    return sb.toString();
  }

  private String retValue(Method m) {
    if (m.getGenericReturnType().equals(Void.TYPE))
      return "null";
    else
      return "$!";
  }
  
  private String args(Method m) {
    StringBuffer sb = new StringBuffer();
    sb.append("new Object[]{");
    int numParms = m.getGenericParameterTypes().length;
    for (int i=0; i<numParms; i++) {
      sb.append("$"+new Integer(i+1).toString());
      if (i<numParms-1)
        sb.append(",");
    }
    sb.append("}");
    return sb.toString();
  }
  
  private String args(Constructor m) {
    StringBuffer sb = new StringBuffer();
    sb.append("new Object[]{");
    int numParms = m.getGenericParameterTypes().length;
    for (int i=0; i<numParms; i++) {
      sb.append("$"+new Integer(i+1).toString());
      if (i<numParms-1)
        sb.append(",");
    }
    sb.append("}");
    return sb.toString();
  }
  
  static ArrayList<Method> methods(Class cl) {
    Method[] declaredMethods = cl.getDeclaredMethods();
    ArrayList<Method> methods = new ArrayList<>();
    for (Method method : declaredMethods) {
      methods.add(method);
    }
    return methods;
  }
  
  static ArrayList<Constructor> constructors(Class cl) {
    Constructor[] declaredConstructors = cl.getDeclaredConstructors();
    ArrayList<Constructor> constructors = new ArrayList<>();
    for (Constructor constructor : declaredConstructors) {
      constructors.add(constructor);
    }
    return constructors;
  }
  
  static ArrayList<Field> fields(Class cl) {
    Field[] declaredFields = cl.getDeclaredFields();
    ArrayList<Field> fields = new ArrayList<>();
    for (Field field : declaredFields) {
      if (Modifier.isStatic(field.getModifiers()))
        System.out.println("\n*** WARNING. Class "+cl+" contains a STATIC FIELD "+field+"\n");
      fields.add(field);
    }
    return fields;
  }
  
  static ArrayList<Field> staticFields(Class cl) {
    ArrayList<Field> staticFields = new ArrayList<>();
    ArrayList<Field> fields = fields(cl);
    for (Field field : fields)
      if (Modifier.isStatic(field.getModifiers()))
        staticFields.add(field);
    return staticFields;
  }
  
  /**
   * Given an interface name the method finds the interface class,
   * and given a directory containing the compiled class files for the solution,
   * it retrieves all user defined classes.
   */
  public static void main(String[] args) {
    if (args.length != 3) {
      System.out.println
        ("\nError: needed arguments missing: name interface class-directory"+
         "\nExample: cc.carretera.Carretera m120029/classes\n");
      System.exit(1);
    }

    String name = args[0];
    String interfaceName = args[1];
    String classDir = args[2];
    Class interfaceClass = null;
    
    try {
      interfaceClass = Class.forName(interfaceName);
    } catch (ClassNotFoundException exc) {
      System.out.println("\nError: cannot find interface "+interfaceName);
      System.exit(1);
    }
    
    if (!interfaceClass.isInterface()) {
      System.out.println("\nError: "+interfaceName+" is not an interface");
      System.exit(1);
    }
    
    
    String packageName = interfaceClass.getPackage().getName();
    String[] parts = packageName.split("\\.");
    String pathSuffix = String.join("/",parts);
    String path = classDir + "/" + pathSuffix;
    String[] classFiles = new File(path).list();

    if (classFiles == null) {
      System.out.println("\nError: could not find any classes in "+path);
      System.exit(1);
    }
    
    // System.out.println("Class files = "+Arrays.toString(classFiles));
    // System.out.println("path = "+path);
    
    PrintStream bytemanFile = null;
    PrintStream prologFile = null;

    Set<Class> userClasses = new HashSet<>();

    for (String classFile : classFiles) {
      if (classFile.endsWith(".class")) {
        String className = classFile.replace(".class","");
        String fullName = null;
        Class userClass = null;
        try {
          fullName = packageName+"."+className;
          userClass = Class.forName(fullName);
          if (!userClass.isInterface()) {
            userClasses.add(userClass);
          }
        } catch (ClassNotFoundException exc1) {
          System.out.println("\nWarning: cannot find class "+fullName);
        }
      }
    }
    
    if (userClasses.size() == 0) {
      System.out.println("\nError: could not find any classes in "+path);
      System.exit(1);
    }

    String interfaceClassName =
      interfaceClass.getSimpleName();
    String interfaceFileName =
      Character.toLowerCase(interfaceClassName.charAt(0)) +
      interfaceClassName.substring(1);

    try {
      bytemanFile = new PrintStream(interfaceFileName+".btm");
      prologFile = new PrintStream(interfaceFileName+".pl");
    } catch (FileNotFoundException exc) {
      throw new RuntimeException();
    }

    RuleGenerator rg =
      new RuleGenerator(name,packageName,interfaceClass,userClasses);

    bytemanFile.println(rg.outputInterfaceRules());
    bytemanFile.println(rg.outputFieldAccessRules());
    // bytemanFile.println(rg.outputFieldCallRules());

    ArrayList<Method> methods = methods(interfaceClass);
    for (Method m : methods) {
      prologFile.println
        ("operation('"+m.getName()+"',"+m.getParameterTypes().length+").");
    }

    bytemanFile.close();
    prologFile.close();
  }
}
