package es.upm.babel.bytemanGenerator;

import java.io.PrintStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileNotFoundException;


public class ByteManUtils {
  private static volatile int counter = 0;
  private static volatile Integer synchOn = new Integer(0);
  private static String buffer;
  private static int identicals = 0;
  private static final int maxIdenticals = 3;
  private static PrintStream outStream = null;
  
  public static void reset() {
    counter = 0;
  }

  public static int getCounter() {
    return counter++;
  }

  public static void startTest() {
    synchronized (synchOn) {
      String str = "startTest()";
      output(str);
    }
  }

  public static void nameTest(String testName) {
    synchronized (synchOn) {
      String str = "test('"+testName+"')";
      output(str);
    }
  }

  public static void makeRead(String className, Object object, String methodName, Object var) {
    synchronized (synchOn) {
      //      String str = "read('"+className+"','@"+System.identityHashCode(object)+"','"+methodName+"',"+printArg(var)+")";
      String str = "read('"+className+"',"+printArg(object)+",'"+methodName+"',"+printArg(var)+")";
      output(str);
    }
  }

  public static void makeWrite(String className, Object object, String methodName, Object var) {
    synchronized (synchOn) {
      //      String str = "write('"+className+"','@"+System.identityHashCode(object)+"','"+methodName+"',"+printArg(var)+")";
      String str = "write('"+className+"',"+printArg(object)+",'"+methodName+"',"+printArg(var)+")";
      output(str);
    }
  }

  public static void makeCall(String className, String methodName, Object[] args) {
    synchronized (synchOn) {
      String str = "call('"+className+"','"+methodName+"',"+printArgs(args)+")";
      output(str);
    }
  }

  public static void makeCallReturn(String className, String methodName, Object[] args, Object returnValue) {
    synchronized (synchOn) {
      String str = "call('"+className+"','"+methodName+"',"+printArgs(args)+")";
      output(returns(str,returnValue));
    }
  }

  public static void makeCallThrow(String className, String methodName, Object[] args, Throwable exception) {
    synchronized (synchOn) {
      String str = "call('"+className+"','"+methodName+"',"+printArgs(args)+")";
      output(raises(str,exception));
    }
  }

  public static void makeNew(String className, Object[] args) {
    synchronized (synchOn) {
      String str = "new('"+className+"',"+printArgs(args)+")";
      output(str);
    }
  }

  public static void makeNewReturn(String className, Object[] args, Object returnValue) {
    synchronized (synchOn) {
      String str = "new('"+className+"',"+printArgs(args)+")";
      output(returns(str,returnValue));
    }
  }

  private static String returns(String call, Object returnValue) {
    String returnValueStr = returnValue == null ? "null" : returnValue.toString();
    String str = "returns("+call+","+printArg(returnValueStr)+")";
    return str;
  }

  private static String raises(String call, Throwable exception) {
    String str = "throws("+call+","+printArg(exception.getClass().getName())+")";
    return str;
  }

  private static String eventAt(String event) {
    return
      "eventAt("+
      getCounter()+",'"+
      Thread.currentThread().getName()+"',"+
      event+
      ").";
  }
  
  private static String printArgs(Object[] args) {
    StringBuffer s = new StringBuffer();
    s.append("[");
    int i=0;
    while (i < args.length) {
      if (i>0) s.append(",");
      s.append(printArg(args[i++]));
    }
    s.append("]");
    return s.toString();
  }

  private static String printArg(Object arg) {
    if (arg == null) return "null";
    if ((arg instanceof Integer)
        || (arg instanceof Short)
        || (arg instanceof Byte)
        || (arg instanceof Long)
        || (arg instanceof Double)
        || (arg instanceof Float)
        || (arg instanceof Boolean))
      return arg.toString();
    else if (arg instanceof String) return "'"+arg.toString()+"'";
    else return "'@"+System.identityHashCode(arg)+"'";
    // else return "'"+arg.toString()+"'";
  }

  private static void output(String msg) {
    synchronized (synchOn) {
      if (outStream == null) {
        try {
          outStream =
            new PrintStream
            (new BufferedOutputStream
             (new FileOutputStream
              (new File
               ("byteman_log.txt"))));
        } catch (FileNotFoundException exc) {
          System.out.println("Could not open byteman_log.txt for output");
          System.exit(1);
        }
      }

      boolean equal =
        (buffer != null) && buffer.equals(msg);

      identicals =
        equal ? identicals+1 : 0;

      buffer =
        equal ? buffer : msg;

      if (identicals < maxIdenticals) {
        outStream.println(eventAt(msg));
        outStream.flush();
      }
    }
  }
}

