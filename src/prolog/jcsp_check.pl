isRead(TimedEvent) :-
    event(TimedEvent,read(_,_,_,_)).

isRead(TimedEvent) :-
    event(TimedEvent,read(_,_,_,_)).

isChannelRead(TimedEvent) :-
    event(TimedEvent,Event),
    callClass(Event,'org.jcsp.lang.AltingChannelInputImpl'),
    callMethod(Event,'read').

isChannelWrite(TimedEvent) :-
    event(TimedEvent,Event),
    callClass(Event,'org.jcsp.lang.SharedChannelOutputImpl'),
    callMethod(Event,'write').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prop_op_reads_attribute(OpMethod,AllReads) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    include(isRead,OpInsts,AllReads),
    length(AllReads,L),
    L > 0,
    callMethod(Call,OpMethod).

prop_op_reads_attribute_printer(Program,Prop,_Ops,Results) :-
    format('~n*** WARNING: ~w satisfies ~w but should not: ~n',[Program,Prop]),
    maplist(print_reads_in_call,Results,_).

print_reads_in_call(result(Op,Errors),_) :-
    maplist(getRead,Errors,PreActions),
    sort(PreActions,Actions),
    format('  operation ~w reads ~w~n',[Op,Actions]).

prop_cpreTrue_and_without_return_op_reads_from_channel(OpMethod,AllReads) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    cpreTrue(OpMethod),
    notReturnsValue(OpMethod),
    include(isChannelRead,OpInsts,AllReads),
    length(AllReads,L),
    L > 0.

%% Unless an operation may throw an exception before evaluating its CPRE (mayThrowBeforeMonitor)
%% the operation has at least a write.

prop_zero_writes(OpMethod,OpCall) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,Return,OpInsts),
    Return \== blocking,
    callMethod(Call,OpMethod),
    include(isChannelWrite,OpInsts,AllWrites),
    length(AllWrites,L),
    (L == 0 -> \+ mayThrowEarlyException(OpMethod)).

%% Any read operation is preceded by a write operation.

prop_read_before_write(OpMethod,Insts) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    before(isChannelWrite,OpInsts,Insts),
    include(isChannelRead,Insts,AllReads),
    length(AllReads,L),
    L > 0.
    
%% At most one read operation

prop_more_than_one_read(OpMethod,reads(OpCall,AllReads)) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    include(isChannelRead,OpInsts,AllReads),
    length(AllReads,L),
    L > 1.

%% At most one write operation
prop_more_than_one_write(OpMethod,AllWrites) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    include(isChannelWrite,OpInsts,AllWrites),
    length(AllWrites,L),
    L > 1.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

check_ops :-
    Props = [ prop_op_reads_attribute, prop_cpreTrue_and_without_return_op_reads_from_channel, prop_more_than_one_read, prop_more_than_one_write, prop_zero_writes, prop_read_before_write, prop_nonzero_operation_calls_in_operation ],
    check_ops(Props).

check_tests.

    
    
