before_enter(OpCall,Insts) :-
    isOpExec(OpCall),
    OpCall = opCall(_,_,OpInsts),
    before(isMonitorEnterCall,OpInsts,Insts).

isMonitorEnterCall(TimedEvent) :-
    event(TimedEvent,Event),
    callClass(Event,'es.upm.babel.cclib.Monitor'),
    callMethod(Event,'enter').

isMonitorCreate(TimedEvent) :-
    event(TimedEvent,Event),
    newClass(Event,'es.upm.babel.cclib.Monitor').

isAwaitCall(TimedEvent) :-
    event(TimedEvent,Event),
    callClass(Event,'es.upm.babel.cclib.Monitor$Cond'),
    callMethod(Event,'await').

isSignalCall(TimedEvent) :-
    event(TimedEvent,Event),
    callClass(Event,'es.upm.babel.cclib.Monitor$Cond'),
    callMethod(Event,'signal').

isMonitorLeaveCall(TimedEvent) :-
    event(TimedEvent,Event),
    callClass(Event,'es.upm.babel.cclib.Monitor'),
    callMethod(Event,'leave').

isMonitorLeaveReturn(TimedEvent) :-
    event(TimedEvent,Event),
    returnCall(Event,Call),
    callClass(Call,'es.upm.babel.cclib.Monitor'),
    callMethod(Call,'leave').

monitorInEvent(TimedEvent,some(This)) :-
    event(TimedEvent,Event),
    callClass(Event,'es.upm.babel.cclib.Monitor'), !,
    callThis(Event,This).

monitorInEvent(TimedEvent,none) :-
    event(TimedEvent,_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 0, 2 or more monitor enter calls in one op.

prop_not_exactly_one_enter(OpMethod,Enters) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    include(isMonitorEnterCall,OpInsts,Enters),
    length(Enters,Len),
    callMethod(Call,OpMethod),
    (Len == 0 -> \+ mayThrowEarlyException(OpMethod);
     Len \== 1).

%% more than one leave in an op.

prop_more_than_one_leave(OpMethod,Leaves) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    include(isMonitorLeaveCall,OpInsts,Leaves),
    length(Leaves,Len),
    Len > 1,
    callMethod(Call,OpMethod).
    
%% if the op returns, not exactly one leave.

prop_return_implies_not_exactly_one_leave(OpMethod,leaves(OpCall,Leaves)) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,Return,OpInsts),
    Return \== blocking,
    include(isMonitorLeaveCall,OpInsts,Leaves),
    length(Leaves,Len),
    callMethod(Call,OpMethod),
    (Len == 0 -> \+ mayThrowEarlyException(OpMethod);
     Len \== 1).

%% same number of enters as leaves

prop_number_of_enters_and_leaves_differs(OpMethod,leaves(OpCall,Enters,Leaves)) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,Return,OpInsts),
    Return \== blocking,
    include(isMonitorEnterCall,OpInsts,Enters),
    include(isMonitorLeaveCall,OpInsts,Leaves),
    length(Leaves,LenLeaves),
    length(Enters,LenEnters),
    LenLeaves \== LenEnters,
    callMethod(Call,OpMethod).

%% Accessing state before acquiring monitor

prop_access_before_monitor(OpMethod,Insts) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    before(isMonitorEnterCall,OpInsts,Insts),
    Insts \== [],
    callMethod(Call,OpMethod).

prop_access_before_monitor_printer(Program,Prop,_Ops,Results) :-
    format('~n*** WARNING: ~w satisfies ~w but should not: ~n',[Program,Prop]),
    maplist(print_readsOrWrites_in_call,Results,_).

print_readsOrWrites_in_call(result(Op,Errors),_) :-
    maplist(getReadOrWrite,Errors,PreActions),
    sort(PreActions,Actions),
    format('  operation ~w has reads/writes ~w~n',[Op,Actions]).

%% Accessing state after leaving monitor

prop_access_after_monitor(OpMethod,Insts) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    after(isMonitorLeaveReturn,OpInsts,PreInsts),
    include(readOrWrite,PreInsts,Insts),
    Insts \== [],
    callMethod(Call,OpMethod).

prop_access_after_monitor_printer(Program,Prop,_Ops,Results) :-
    format('~n*** WARNING: ~w satisfies ~w but should not: ~n',[Program,Prop]),
    maplist(print_readsOrWrites_in_call,Results,_).

%% Two or more awaits

prop_multiple_awaits(OpMethod,Leaves) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    include(isAwaitCall,OpInsts,Leaves),
    length(Leaves,Len),
    Len > 1,
    callMethod(Call,OpMethod).

%% Await in an op with a trivially true CPRE
prop_await_for_trivial_cpre(OpMethod,Leaves) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    include(isAwaitCall,OpInsts,Leaves),
    length(Leaves,Len),
    Len > 1,
    cpreTrue(OpMethod).

%% Zero or more than one monitors created in a test.

prop_not_exactly_one_monitor_in_test(N,SortedMonitors) :-
    testCode(N,C),
    maplist(monitorInEvent,C,PreMonitors),
    include(isSome,PreMonitors,Monitors),
    sort(Monitors,SortedMonitors),
    length(SortedMonitors,Len),
    Len \== 1.

%% prop_not_exactly_one_monitor_in_test(N,Enters) :-
%%    testCode(N,C),
%%    include(isMonitorCreate,C,Enters),
%%    length(Enters,Len),
%%    Len \== 1.

prop_zero_signals_dentro_op(Op,0) :-
    operation(Op,_),
    findall(NumCalls,( isOpExec(opCall(Call,_,OpInsts)), callMethod(Call,Op), include(isSignalCall,OpInsts,AllSignalCalls), length(AllSignalCalls,NumCalls) ), AllNumCalls),
    list_sum(AllNumCalls,SumCalls),
    SumCalls is 0,
    \+ notInChain(Op).

prop_zero_awaits :-
    findall(Event, ( isTimedEvent(Event) ), AllEvents),
    include(isAwaitCall,AllEvents,AllAwaitCalls),
    length(AllAwaitCalls,0).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

check_ops :-
    Props = [prop_not_exactly_one_enter, prop_more_than_one_leave, prop_number_of_enters_and_leaves_differs, prop_return_implies_not_exactly_one_leave, prop_access_before_monitor, prop_access_after_monitor, prop_multiple_awaits, prop_await_for_trivial_cpre, prop_zero_signals_dentro_op, prop_nonzero_operation_calls_in_operation],
    check_ops(Props).

check_tests :-
    Props = [prop_not_exactly_one_monitor_in_test],
    check_tests(Props).

