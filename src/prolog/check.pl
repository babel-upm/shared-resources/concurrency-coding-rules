isSome(some(_)).

time(eventAt(Time,_,_),Time).
thread(eventAt(_,Thread,_),Thread).
event(eventAt(_,_,Event),Event).

isTimedEvent(eventAt(Time,Thread,Event)) :-
    eventAt(Time,Thread,Event).
isStartTest(TimedEvent) :-
    event(TimedEvent,startTest()).
isCall(call(_,_,_)).
isReturns(returns(_,_)).

callClass(call(Class,_,_),Class).
callMethod(call(_,Method,_),Method).
callMethod(eventAt(_,_,call(_,Method,_)),Method).
callThis(call(_,_,[This|_]),This).
callArgs(call(_,_,[_|Args]),Args).
callArity(call(_,_,[_|Args]),Arity) :-
    length(Args,Arity).
unblocksCall(Reason,Call) :-
    returnCall(Reason,Call).
unblocksCall(Reason,Call) :-
    raisesCall(Reason,Call).
returnCall(returns(Call,_),Call).
raisesCall(throws(Call,_),Call).
newClass(new(Class,_),Class).

testStartsAt(Time) :-
    isTimedEvent(TimedEvent),
    isStartTest(TimedEvent),
    time(TimedEvent,Time).

testName(Time,Name) :-
    isTimedEvent(TimedEvent),
    event(TimedEvent,test(Name)),
    time(TimedEvent,Time).

:- table test/3.

test(Name,Start,End) :-
    testStartsAt(Start),
    testStartsAt(NewStart),
    before(Start,NewStart),
    \+ (
            testStartsAt(InBeetween),
            inbetween(Start,InBeetween,NewStart)
        ),
    testName(TestTime,Name),
    inbetween(Start,TestTime,NewStart),
    End is NewStart-1.

test(Name,Start,final) :-
    testStartsAt(Start),
    \+ ( testStartsAt(NewStart), before(Start,NewStart) ),
    testName(TestTime,Name),
    before(Start,TestTime).

testCode(Name,Codes) :-
    test(Name,Start,End),
    findall(Event, ( isTimedEvent(Event), time(Event,EventTime), beforeOrAt(Start,EventTime), beforeOrAt(EventTime,End) ), TestEvents),
    sort(TestEvents,Codes).

opCall(TimedEvent) :-
    isTimedEvent(TimedEvent),
    event(TimedEvent,Event),
    callMethod(Event,Method),
    callClass(Event,Class),
    callArgs(Event,Args),
    length(Args,Len),
    operation_class(Class),
    operation(Method,Len).

opReturn(TimedEvent,TimedEventReturn) :-
    opCall(TimedEvent),
    time(TimedEvent,CallTime),
    event(TimedEvent,Call),
    thread(TimedEvent,Thread),
    isTimedEvent(TimedEventReturn),
    event(TimedEventReturn,EventReturn),
    unblocksCall(EventReturn,Call),
    thread(TimedEventReturn,Thread),
    time(TimedEventReturn,ReturnTime),
    before(CallTime,ReturnTime),
    duringTest(CallTime,TestName),
    duringTest(ReturnTime,TestName).

opPair(CallEvent,ReturnEvent) :-
    opReturn(CallEvent,ReturnEvent).
opPair(CallEvent,blocking) :-
    opCall(CallEvent),
    \+ ( opReturn(CallEvent,_) ).

opExec(opCall(Call,blocking,OpInsts)) :-
    opPair(Call,blocking),
    thread(Call,Thread),
    time(Call,CallTime),
    duringTest(CallTime,TestName),
    test(TestName,_,EndTime),
    findall(Event, ( isTimedEvent(Event), thread(Event,Thread), time(Event,EventTime), before(CallTime,EventTime), before(EventTime,EndTime) ), CallEvents),
    sort(CallEvents,OpInsts).

opExec(opCall(Call,Return,OpInsts)) :-
    opPair(Call,Return),
    Return \== blocking,
    thread(Call,Thread),
    time(Call,CallTime),
    time(Return,ReturnTime),
    findall(Event, ( isTimedEvent(Event), thread(Event,Thread), time(Event,EventTime), before(CallTime,EventTime), before(EventTime,ReturnTime) ), CallEvents),
    sort(CallEvents,OpInsts).

:- table opExecs/1.

opExecs(L) :-
    findall(E, opExec(E), L).

isOpExec(E) :-
    opExecs(L),
    member(E,L).

before(T,final) :- integer(T).
before(T1,T2) :- integer(T1), integer(T2), T1 < T2.

inbetween(T1,T2,T3) :-
    before(T1,T2),
    before(T2,T3).

beforeOrAt(T,T).
beforeOrAt(T1,T2) :-
    before(T1,T2).

duringTest(Time,TestName) :-
    test(TestName,StartTime,EndTime),
    beforeOrAt(StartTime,Time),
    beforeOrAt(Time,EndTime).

solution(N) :-
    Goal = solutionName(N),
    ( current_predicate(_,Goal) ->
      call(Goal);
      N=undefined ).

haltWithErrorStatus :-
    solution(Program),
    ( current_predicate(_,hasError) ->
      format('~nAnalysis of ~w revealed a possible problem.~n',Program),
      halt(1);
      format('~nAnalysis of ~w did not reveal any problem.~n',Program),
      halt(0) ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

before(_,[],[]).
before(G,[Hd|_],[]) :-
    call(G,Hd).
before(G,[Hd|Tl],[Hd|Rest]) :-
    \+ ( call(G,Hd) ),
    before(G,Tl,Rest).

after_leave(OpCall,Insts) :-
    isOpExec(OpCall),
    OpCall = opCall(_,_,OpInsts),
    after(isMonitorLeaveCall,OpInsts,Insts).

after(_,[],[]).
after(G,[Hd|Tl],Tl) :-
    call(G,Hd).
after(G,[Hd|Tl],L) :-
    \+ ( call(G,Hd) ),
    after(G,Tl,L).

list_sum([],0).
list_sum([Hd|Tl],N) :-
    list_sum(Tl,N1),
    N is Hd + N1.

isOpCall(TimedEvent) :-
    event(TimedEvent,Event),
    callClass(Event,Class),
    operation_class(Class),
    callMethod(Event,Method),
    callArity(Event,Arity),
    operation(Method,Arity).

getOp(TimedEvent,Method) :-
    event(TimedEvent,Event),
    callMethod(Event,Method).

readOrWrite(TimedEvent) :-
    event(TimedEvent,read(_,_,_,_)).
readOrWrite(TimedEvent) :-
    event(TimedEvent,write(_,_,_,_)).

getRead(TimedEvent,read(Method,Var)) :-
    event(TimedEvent,read(_,_,Method,Var)).
getWrite(TimedEvent,read(Method,Var)) :-
    event(TimedEvent,write(_,_,Method,Var)).

getReadOrWrite(TimedEvent,Action) :-
    getRead(TimedEvent,Action).
getReadOrWrite(TimedEvent,Action) :-
    getWrite(TimedEvent,Action).

prop_nonzero_operation_calls_in_operation(OpMethod,OpCalls) :-
    isOpExec(OpCall),
    OpCall = opCall(Call,_,OpInsts),
    callMethod(Call,OpMethod),
    include(isOpCall,OpInsts,OpCalls),
    length(OpCalls,Len),
    Len >= 1.

prop_nonzero_operation_calls_in_operation_printer(Program,Prop,_Ops,Results) :-
    format('~n*** WARNING: ~w satisfies ~w but should not: ~n',[Program,Prop]),
    maplist(print_op_calls_in_call,Results,_).

print_op_calls_in_call(result(Op,Errors),_) :-
    maplist(getOp,Errors,PreCalls),
    sort(PreCalls,Calls),
    format('  operation ~w calls operations ~w~n',[Op,Calls]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

failing_ops_printer(Program,Prop,Ops) :-
    format('~n*** WARNING: ~w: the operations ~w satisfy the property ~w but should not~n',[Program,Ops,Prop]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num_successful_calls(Op,Len) :-
    operation(Op,_),
    findall(Call,( isOpExec(opCall(Call,Return,_)), Return \== blocking, event(Call,Event), callMethod(Event,Op) ),List),
    length(List,Len).

check_ops([]).
check_ops([P|Rest]) :-
    check_ops_pred(P),
    check_ops(Rest).

check_program :-
    solution(Program),
    format('~n~nChecking if sensible execution~n'),
    ( findall(Op, ( num_successful_calls(Op,N), N<5 ), Ops),
      Ops \== [] ->
      format('~n~w: *** WARNING: the operations ~w are not called successfuly enough times; probably the checked program is very buggy. NOTE THAT AS A RESULT SOME OTHER DIAGNOSTIC RESULTS MAY BE SUSPECT.~n',[Program,Ops]),
      assert(hasError);
      true).

check_ops_pred(P) :-
    solution(Program),
    %% format('~n~nChecking property ~w~n',[P]),
    findall(result(Op,Error),distinct(Op,call(P,Op,Error)),Results),
    ( Results\==[] ->
      maplist(result_entity,Results,Ops),
      operation_error_printer(Program,P,Ops,Results),
      assert(hasError);
      true).
check_ops_pred(_).

operation_error_printer(Program,P,Ops,Results) :-
    atom_concat(P,'_printer',Printer),
    (current_functor(Printer,4),
     call(Printer,Program,P,Ops,Results) ->
         true;
     (current_prolog_flag(argv,Args),
      member(debug,Args) ->
          failing_ops_printer(Program,P,Ops),
          format('~ndebugging: results=~n~w~n',[Results]);
      failing_ops_printer(Program,P,Ops))).

check_tests([]).
check_tests([P|Rest]) :-
    check_test_pred(P),
    check_tests(Rest).

check_test_pred(P) :-
    solution(Program),
    %% format('~n~nChecking property ~w~n',[P]),
    findall(result(Test,Error),distinct(Test,call(P,Test,Error)),Results), 
    ( Results\==[] ->
      maplist(result_entity,Results,Tests),
      format('~n~w: *** WARNING: property ~w: the tests ~w satisfy the property but should not.~n',[Program,P,Tests]),
      format('*** Diagnostics: ~w~n',[Results]),
      assert(hasError);
      true).
check_test_pred(_).
    
result_entity(result(Entity,_),Entity).




    
    
    
    

