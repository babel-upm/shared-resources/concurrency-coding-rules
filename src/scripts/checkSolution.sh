#!/bin/bash

INTERFACE=cc.banco.Banco
LIBDIR=../libs

if [ -z "$BYTEMAN_HOME" ]; then
    echo "ERROR: the variable BYTEMAN_HOME is not set; set it to the directory where byteman was downloaded."
    exit 1
fi

if [ $# -lt 3 ]
then
	echo "Usage: $0 [mon|csp] entrega-dir name [-debug]"
	exit 1
fi

MonCSP=$1
shift
EntregaDir=$1
shift
Name=$1
shift

if [ $# -lt 1 ]
then
    Debug=""
else
    Debug=$1
fi

if [[ "$MonCSP" =~ ^mon$ ]]; then
    IsAMonitor=true
    RuleGenerator=RuleGenerator
    Rules=rules/cclibRules.btm 
    TESTER=cc.banco.TestSecondMon
else
   IsAMonitor=false
    RuleGenerator=CSPRuleGenerator
    Rules=rules/jcspRules.btm
    TESTER=cc.banco.TestSecondCSP
fi

mkdir -p runs genRules typecheck results

CLASSPATH=classes:../../classes:$LIBDIR/aedlib-2.8.1.jar:$LIBDIR/sequenceTester.jar:$LIBDIR/cclib.jar:$LIBDIR/jcsp.jar

# Now generate byteman rules from the entrega (class files)
java -cp $EntregaDir/classes:$CLASSPATH es.upm.babel.bytemanGenerator.$RuleGenerator $EntregaDir $INTERFACE $EntregaDir/classes

# Combine the entrega specific byteman rules with the general rules for monitor resources
cat banco.btm $Rules > genRules/rules_"$Name".btm

# Check that the generated rules are syntactically sound
$BYTEMAN_HOME/bin/bmcheck.sh -cp $EntregaDir/classes:$CLASSPATH genRules/rules_"$Name".btm > typecheck/typecheck_"$Name".txt
if grep --quiet ERROR typecheck/typecheck_"$Name".txt; then
    echo "Byteman rules are NOT syntactically correct; consult typecheck_$Name.txt for details."
    exit 1
fi

# Next run a test suite, while tracing tests according to the byteman rules
BYTEMAN_JAR=${BYTEMAN_HOME}/lib/byteman.jar
echo claspath is $CLASSPATH
echo Executing "java -javaagent:${BYTEMAN_JAR}=boot:${BYTEMAN_JAR},script:genRules/rules_"$Name".btm -jar $LIBDIR/junit-platform-console-standalone.jar --class-path $EntregaDir/classes:$CLASSPATH --details=summary -c $TESTER >& results/result_"$Name""
java -javaagent:${BYTEMAN_JAR}=boot:${BYTEMAN_JAR},script:genRules/rules_"$Name".btm -jar $LIBDIR/junit-platform-console-standalone.jar --class-path $EntregaDir/classes:$CLASSPATH --details=summary -c $TESTER >& results/result_"$Name"

# Select trace events
cp byteman_log.txt runs/"run_"$Name".pl"

# Finally, check the trace events against the correctness predicates in SWI Prolog
bash scripts/prologCheck.sh $IsAMonitor "runs/run_"$Name".pl" "$Name" "$Debug"



