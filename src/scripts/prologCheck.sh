#!/bin/bash

if [ $# -lt 4 ]
then
	echo "Usage: $0 isMonitor program_base.pl name [-debug]"
	exit 1
fi

IsAMonitor=$1
shift
Program=$1
shift
Name=$1
shift
DebugOpt=$1
shift

case $DebugOpt in
    "-debug") UserArgs="--"; Debug="debug";;
    *) ;;
esac

if [[ $IsAMonitor == "true" ]]; then
    Rules=cc_check.pl
else
    Rules=jcsp_check.pl
fi

# Check the trace events against the correctness predicates in SWI Prolog

swipl ${Debug:+"--"} ${Debug:+"$Debug"} <<EOF
consult("prolog/check.pl"), consult("prolog/$Rules"), consult("op_info.pl"), consult("$Program").
assert(solutionName("$Name")).
check_program, check_ops, check_tests, haltWithErrorStatus.
haltWithErrorStatus.
EOF
